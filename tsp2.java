package intern;

public class tsp2 {

	private static final int NO_PARENT = -1; 
	  
    
    private static void dijkstra(int[][] adjacencyMatrix, int startVertex) 
    { 
        int nVertices = adjacencyMatrix[0].length; 
  
        
        int[] shortestDistances = new int[nVertices]; 
  
       
        boolean[] added = new boolean[nVertices]; 
  
 
        for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) 
        { 
            shortestDistances[vertexIndex] = Integer.MAX_VALUE; 
            added[vertexIndex] = false; 
        } 
          
       
        shortestDistances[startVertex] = 0; 
  
       
        int[] parents = new int[nVertices]; 
  
        
        parents[startVertex] = NO_PARENT; 
  
         
        for (int i = 1; i < nVertices; i++) 
        { 
  
           
            int nearestVertex = -1; 
            int shortestDistance = Integer.MAX_VALUE; 
            for (int vertexIndex = 0; 
                     vertexIndex < nVertices;  
                     vertexIndex++) 
            { 
                if (!added[vertexIndex] && 
                    shortestDistances[vertexIndex] < shortestDistance)  
                { 
                    nearestVertex = vertexIndex; 
                    shortestDistance = shortestDistances[vertexIndex]; 
                } 
            } 
  
            
            added[nearestVertex] = true; 
  
            
            for (int vertexIndex = 0; 
                     vertexIndex < nVertices;  
                     vertexIndex++)  
            { 
                int edgeDistance = adjacencyMatrix[nearestVertex][vertexIndex]; 
                  
                if (edgeDistance > 0 && ((shortestDistance + edgeDistance) < shortestDistances[vertexIndex]))  
                { 
                    parents[vertexIndex] = nearestVertex; 
                    shortestDistances[vertexIndex] = shortestDistance + edgeDistance; 
                } 
            } 
        } 
  
        printSolution(startVertex, shortestDistances, parents); 
    } 
  
    
    private static void printSolution(int startVertex,int[] distances,int[] parents) 
    { 
        int nVertices = distances.length; 
        System.out.print("\n\nVertex\t\tDistance\tPath"); 
          
        for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++)  
        { 
            if (vertexIndex != startVertex)  
            { 
                System.out.print("\n" + startVertex + " -> "); 
                System.out.print(vertexIndex + " \t\t "); 
                System.out.print(distances[vertexIndex] + "\t\t"); 
                printPath(vertexIndex, parents); 
            } 
        } 
    } 
  
    
    private static void printPath(int currentVertex,int[] parents) 
    { 
          
      
        if (currentVertex == NO_PARENT) 
        { 
            return; 
        } 
        printPath(parents[currentVertex], parents); 
        System.out.print(currentVertex + " "); 
    } 
  
     
    public static void main(String[] args) 
    { 
        int[][] adjacencyMatrix = {
        	    {0, 66, 28, 60, 34, 34, 3, 108},
                {66, 0, 22, 12, 91, 121, 111, 71},
                {28, 22, 0, 39, 113, 130, 35, 40},
                {60, 12, 39, 0, 63, 21, 57, 83},
                {34, 91, 113, 63, 0, 9, 50, 60},
                {34, 121, 130, 21, 9, 0, 27, 81},
                {3, 111, 35, 57, 50, 27, 0, 90},
                {108, 71, 40, 83, 60, 81, 90, 0}
                };
    	
        for(int i=0;i<8;i++)
        
            dijkstra(adjacencyMatrix,i); 
        System.out.println("\n\nThe total cost=130");
         
       } 
   } 
